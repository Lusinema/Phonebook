﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MicPhoneBook
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Contacts> contactList = CreateContacts(20);
            for (int i = 0; i < contactList.Count; i++)
                Console.WriteLine(contactList[i].FullName);

        PhoneBook phoneCode = new PhoneBook();
        List<Contacts> codeSearch = phoneCode.Finder("093", contactList);

            for (int i = 0; i<codeSearch.Count; i++)
                Console.WriteLine(codeSearch[i].FullName);
            Console.ReadLine();
        }

    private static List<Contacts> CreateContacts(int count)
    {
        string[] operators = { "093", "095", "091" };//kisat
        var rnd = new Random();

        PhoneBook code = new PhoneBook();
        List<Contacts> contacts = new List<Contacts>(count);
        for (int i = 0; i < count; i++)
        {
            int item = rnd.Next(0, operators.Length - 1);
            string number = $"{operators[item]}{rnd.Next(100000, 999999)}";
            bool check = code.IfExists(number, contacts);
            if (check)
            {
                var contact = new Contacts();
                contact.Number = number;
                contact.Name = $"A{i}";
                contact.Surname = $"A{i}yan";
                contact.Email = $"a{i}@gmail.com";
                contacts.Add(contact);
            }
            else
                i--;
        }
        return contacts;
    }


}
}
