﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MicPhoneBook
{
    class Contacts
    {
        public string Name { get; set; }
        public string Surname { get; set; }

        protected string number;
        public string Number
        {
            get { return number; }
            set
            {
                if (value.StartsWith("093") || value.StartsWith("095") || value.StartsWith("091"))
                {
                    number = value;
                }
                else
                    Console.WriteLine("Please enter correct mail");
            }
        }

        protected string email;
        public string Email
        {
            get => email;
            set
            {
                if (value.EndsWith("@gmail.com"))
                    email = value;
                else
                    Console.WriteLine("Please enter correct mail");
            }
        }
        public string FullName => $"{Name}_{Surname}_{Email}_{Number}";
           
}
}
