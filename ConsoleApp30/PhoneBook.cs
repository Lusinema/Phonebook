﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MicPhoneBook
{
    class PhoneBook:Contacts
    {
        public bool IfExists(string num, List<Contacts> contactList)
        {
            for (int i = 0; i < contactList.Count; i++)
            {
                if (num == contactList[i].Number)
                {
                    return false;
                }
            }
            return true;
        }
        public List<Contacts> Finder(string codes, List<Contacts> contactList)
        {
            List<Contacts> list = new List<Contacts>();
            for (int i = 0; i < contactList.Count; i++)
            {
                if (!contactList[i].Number.StartsWith(codes))
                {
                    list.Add(contactList[i]);
                }
            }
            return list;
        }

    }
}
